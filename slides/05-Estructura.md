# Estructura.

---V

#### **\documentclass{}**

1. **Diferentes Estilos de Documentos:**
   - Los estilos de documentos en LaTeX determinan la estructura y el formato del documento que estás creando.
   - La elección del estilo depende del tipo de documento que necesitas crear y la organización que requiere.

---V

#### **\documentclass{}**

2. **Clases Típicas de Documento:**
   - `article`, `book`, `report`, `memoir` y `beamer` (presentaciones).

3. **Definición de Clases y Opciones:**
   - Ejemplos de opciones incluyen `a4paper`, `10pt`, `twoside`, `titlepage`, `fleqn`, entre otras.

---
```latex
    \documentclass[a4paper, 10pt, conference, twocolumn]{article}
```
---V

#### **Autor, titulo y fecha**

- **\title{...}:**  Establece el título del documento.
    - Uso: \title{Mi Título}

- **\author{...}:** Define el nombre del autor del documento LaTeX.
    - Uso: \author{Nombre del Autor}

- **\date{...}:** Establece la fecha del documento.
    - Uso: \date{Fecha}
---V
#### **Autor, titulo y fecha: Ejemplo**
```latex
    \title{Preparación de informes en LaTeX\\
    {\large UNLP}}
    \author{Albert Einstein}
    \date{Agosto 2023}
```
---V
#### **\begin**
- "\begin" inicia entornos de formato. Los entornos son estructuras que formatean partes específicas del documento.
- Sintaxis general: 
```latex
\begin{nombre_del_entorno}
    Contenido del entorno
\end{nombre_del_entorno}
````
---V
#### **\begin: ejemplos**
- `\begin{document} ... \end{document}` para el contenido principal.
- `\begin{itemize} ... \end{itemize}` para listas no numeradas.
- `\begin{enumerate} ... \end{enumerate}` para listas numeradas.
- `\begin{table} ... \end{table}` para crear tablas.
- `\begin{figure} ... \end{figure}` para insertar imágenes.
---V
#### *Inicio de archivo*
```latex
\begin{document}

\maketitle

\section{Introducción}

\end{document}
```
---V
#### **Comandos de Sección**
- Estructuran y organizan el contenido del documento en niveles jerárquicos.
- `\section{Nombre de la sección}` crea una nueva sección numerada.
- `\subsection{Nombre de la subsección}` crea una subsección dentro de una sección.
- `\paragraph{Nombre del párrafo}` inicia un párrafo con un título.
---V
#### **Personalización de Secciones**
- Los títulos de secciones y subsecciones son automáticamente numerados.
- Agregando `*` podemos crear secciones no numeradas, como \section*{Título}.
- Se pueden añadir etiquetas para referencias cruzadas: *\section[label]{Título}*.
---V
#### **Paquetes**

- El comando `\usepackage` se utiliza para cargar paquetes en un documento LaTeX.
- Estos paquetes agregan características específicas al documento.
- La sintaxis básica es: `\usepackage[opciones]{nombre_del_paquete}`.

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}  % Para caracteres especiales
\usepackage{graphicx}       % Para incluir imágenes
\usepackage{amsmath}        % Para notación matemática
```
---V
#### **Opciones de Paquetes**
- Algunos paquetes admiten opciones personalizables.
- Estas opciones afectan el comportamiento o la apariencia del paquete.
- Ejemplo: *`\usepackage[spanish]{babel}`* configura el lenguaje del documento.
