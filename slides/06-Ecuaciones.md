# Ecuaciones.
---V
## Reglas básicas de sintaxis
---V

#### **1. Encapsulación de Ecuaciones**
- `$ ... $` para ecuaciones en línea: `$f(x) = x^2$`.
- `\[ ... \]`, `$$ ... $$` o `\begin{equation}` para ecuaciones en bloque.

---V
#### **2. Símbolos y Operadores**
- Se utilizan símbolos matemáticos como `+`, `-`, `*`, `/`, `=`.
- Para fracciones se utiliza: `\frac{num}{den}`.

---V
#### **3. Superíndices e Índices**
- `x^2` para superíndices: \(x^2\).
- `x_1` para índices: \(x_1\).
- Las llaves `{}` sirven para encapsular texto en subíndices, superíndices y otros contextos.
- Ejemplo: \(a_{\text{max}}\) para subíndice con texto "max".

---V
#### **4. Raíces y Exponentes**
- `\sqrt{2}` para raíz cuadrada: \(\sqrt{2}\).
- `a^{n^2}` para exponentes anidados: \(a^{n^2}\).

---V
#### **5. Ecuaciones Matriciales**
- Añada el paquete "amsmath" (American Mathematical Society)
- Utilice el entorno `bmatrix` : `\begin{bmatrix} ... \end{bmatrix}`.

```latex
$$
\begin{bmatrix}
    1 & 2 & 3\\
    4 & 5 & 6\\
    7 & 8 & 9\\
\end{bmatrix}
$$
```

---V
#### **6. Símbolos Especiales**
- `\alpha`, `\beta`, `\sum`, `\int`, `\infty`, `\rightarrow`, etc.
---V

## Ecuaciones en bloque.

---V
#### **\begin{equation}**

- `\begin{equation}` y `\end{equation}` encapsulan ecuaciones para numeración automática.
- Cada ecuación en este entorno se etiqueta con un número.
- Los números de ecuación se actualizan automáticamente si se agrega o quita una ecuación.
- Se puede referenciar ecuaciones en el texto usando `\label` y `\ref`.

- Ejemplo:
---V
#### **Ejemplo de \begin{equation}**

```latex
La Ecuación de Einstein de la Relatividad Especial (ver ec.\ref{eq:einstein})
\begin{equation}\label{eq:einstein}
    E = mc^2
\end{equation}
```
---V
#### **Múltiples ecuaciones en un mismo bloque**
- Para esto utilizamos el entrono **"*align*"**.
- Al **finalizar una linea** debemos escribir **"\\\\"** para que el compilador lo entienda.
- Podemos utilizar **'&='** para **alinear** los signos igual.
- Se le dará un **número diferente de ecuación** a cada linea.

```latex
\begin{align}
    e &= \lim_{n\to\infty} \left(1 + \frac{1}{n}\right)^n\\
    &= \lim_{n\to\infty} \frac{n}{\sqrt[n]{n!}}
\end{align}
```

---V
#### **Múltiples ecuaciones con una única referencia**
- En este caso creamos la ecuación en un entorno **equation**.
- Dentro del entorno creamos un sub-entorno **split**
- Ubicados en el sub-entorno escribimos nuestra ecuación.
- Alternativamente se puede simplemente agregar **"\\nonumber"** (pero la ref. no aparecerá centrada).
```latex
\begin{equation}
    \begin{split}
        b &= |a + jb|\\
        &= \sqrt{a^2 + b^2}
    \end{split}
\end{equation}
```
---V
#### **Partir ecuación larga**
- Para partir una ecuación utilizamos el entorno **multiline**
- Simplemente insertamos \\\\ donde queramos partir la ecuación.
- La ecuación resultante tendrá una **única referencia**.

```latex
\begin{multline}
\cos(x) = 1 - \frac{x^2}{2!} + \frac{x^4}{4!} - \frac{x^6}{6!} + \frac{x^8}{8!} - \frac{x^{10}}{10!} + \frac{x^{12}}{12!} - \frac{x^{14}}{14!} \\
= 1 - \frac{1}{2!}x^2 + \frac{1}{4!}x^4 - \frac{1}{6!}x^6 + \frac{1}{8!}x^8 - \frac{1}{10!}x^{10} + \frac{1}{12!}x^{12} - \frac{1}{14!}x^{14}
\end{multline}
```
