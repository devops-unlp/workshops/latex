# Apéndice
---V
### Saltos de Línea y Espaciado

- `\\`: Crea un salto de línea.
- `\\[espacio]`: Crea un salto de línea con un espacio adicional.
- `\\*[espacio]`: Similar a `\\[espacio]`, pero evita que una página termine en ese punto.
- `\vspace{[longitud]}`: Agrega espacio vertical.
- `\hspace{[longitud]}`: Agrega espacio horizontal.

---V
### Fuente y Tamaño de Texto

- `\textbf{Texto}`: Muestra el texto en negrita.
- `\textit{Texto}`: Muestra el texto en cursiva.
- `\texttt{Texto}`: Muestra el texto en tipo de letra monoespaciada.
- `\underline{Texto}`: Subraya el texto.
- `\textsuperscript{Texto}`: Eleva el texto a modo de superíndice.
- `\textsubscript{Texto}`: Desciende el texto a modo de subíndice.

---V
### Tamaño de Fuente

- `\tiny`, `\scriptsize`, `\footnotesize`, `\small`, `\normalsize`, `\large`, `\Large`, `\LARGE`, `\huge`, `\Huge`: Cambian el tamaño de fuente.

---V
### Listas y Enumeraciones

- `itemize`: Crea una lista con viñetas.
- `enumerate`: Crea una lista numerada.
- `description`: Crea una lista con descripciones.

---V
### Tablas

- `tabular`: Crea una tabla.
- `\\`: Cambia de fila en una tabla.
- `&`: Separador de columnas en una tabla.
- `\hline`: Línea horizontal en una tabla.
- `|`: Barra vertical para bordes en una tabla.

---V
### Referencias Cruzadas y Citas

- `\label{etiqueta}`: Etiqueta un elemento para referencias cruzadas.
- `\ref{etiqueta}`: Muestra el número de la sección, figura, tabla, etc., etiquetado previamente.
- `\cite{referencia}`: Cita una referencia bibliográfica.

---V
### Ecuaciones

- `$...$` o `\(...\)`: Para ecuaciones en línea.
- `\[...\]` o `\begin{equation}...\end{equation}`: Para escribir fórmulas matemáticas en una línea separada.
- `_`: Subíndice en modo matemático.
- `^`: Superíndice en modo matemático.
- `\frac{numerador}{denominador}`: Fracción.
- `\sqrt{...}`: Raíz cuadrada.
- `\sqrt[]{...}`: Raíz n-esima.

---V
### Ecuaciones II 
- `\sum_{inicio}^{fin}`: Sumatoria.
- `\int_{a}^{b}`: Integral definida.
- `\iiint`: Integral triple(requiere paquete amsmath)
- `\quad`: Inserta cuatro espacios dentro de la ecuación.
- `\qquad`: Da un poco mas de espacio.
- `\theta`, `\pi`, `\...`: Caracteres griegos.
- https://www.cmor-faculty.rice.edu/~heinken/latex/symbols.pdf
