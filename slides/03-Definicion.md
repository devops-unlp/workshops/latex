# LaTeX y un poco de historia.

---V
#### **¿Qué es?**

- Sistema de composición de documentos científicos y técnicos.
- Utiliza lenguaje de marcado.
- Automatiza tareas complejas como numeración y referencias.
- Facilita colaboración en línea (por ejemplo, Overleaf).

---V
#### **Breve historia de LaTeX:**
- Desarrollado por Leslie Lamport en los años 80.
- Basado en el sistema TeX de Donald Knuth (años 70).
- Creado para simplificar creación de documentos científicos.
- Proporciona capa de abstracción sobre comandos TeX.

---V
#### **¿Cómo lo utilizo? (método avanzado)**

1. **Instalar una "TeX Distribution".**
    - Ej: TexLive (multiplataforma)

2. **Editor de Texto.**
    - ¡Incluso MS notepad sirve!.

3. **Escribir el Código LaTeX.**

5. **Compilar.**

---V
#### **¿Cómo lo utilizo? (principiantes)**
# Overleaf
