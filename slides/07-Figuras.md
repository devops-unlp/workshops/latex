# Figuras.

---V
#### **Figuras en LaTeX**

- Para añadir figuras a nuestro documento debemos incluir el paquete **graphicx**
- Las figuras (y otros elementos en LaTeX) se dividen en **flotantes** y **no flotantes**
- En las flotantes, LaTeX maneja la colocación automáticamente.
- Las no flotantes son de utilidad cuando necesites control preciso sobre la ubicación.
- Se aconseja etiquetar con `\label` para hacer referencia a ellas en el texto.

---V
#### **Figuras Flotantes:**

- Las figuras flotantes son objetos que LaTeX coloca automáticamente dentro del documento.
- LaTeX gestiona la posición de las figuras según su contenido y espacio disponible.
- Evitan interrupciones en el flujo de texto.
- Mejoran la distribución de figuras en el documento.
- LaTeX asigna números y genera listas de figuras automáticamente.

---V
#### **Uso de Figuras Flotantes:**
```latex
\begin{figure}[posición]
    \centering
    \includegraphics[opciones]{nombre_del_archivo}
    \caption{Descripción de la figura.}
    \label{fig:nombre_etiqueta}
\end{figure}
```
---V
#### **Posición de las figuras flotantes**
1. `[h]` (Here): Coloca la figura cerca del código fuente.
2. `[t]` (Top): Coloca la figura en la parte superior de una página.
3. `[b]` (Bottom): Coloca la figura en la parte inferior de una página.
4. `[p]` (Page): Coloca la figura en una página separada para figuras y tablas.

---V
#### **Figuras No Flotantes:**

- Las figuras no flotantes se insertan directamente en el lugar.
- No están sujetas a la colocación automática de LaTeX.
- Para insertarlas ejecutamos la instrucción de \includegraphics por fuera del entorno figure.
- En algunas ocasiones funciona incluir en gráfico dentro de un entorno **centering**

*Uso de Figuras No Flotantes:*
```latex
\includegraphics[opciones]{nombre_del_archivo}
```
