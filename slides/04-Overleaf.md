# Overleaf.

---V

#### **¿Qué es Overleaf?**
- Overleaf es una plataforma en línea para la edición y colaboración de documentos LaTeX.
- Proporciona un entorno de edición en tiempo real que permite a múltiples usuarios trabajar juntos en un mismo documento.
- Elimina la necesidad de instalar una distribución de TeX en tu ordenador local.
---V

#### **Ventajas de Overleaf:**
- Acceso en línea.

- Colaboración en tiempo real.

- Plantillas.

- Control de versiones.

- Sintaxis y resaltado de código.

- Referencias y citas.

- Previsualización instantánea.
