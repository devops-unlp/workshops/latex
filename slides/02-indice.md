# Índice.

<small>

1. **¿Qué es LaTeX? Definición y un poco de historia.**
2. **Creación de artículos utilizando Overleaf.**
3. **Estructura del artículo (Título, secciones, subsecciones).**
4. **Ecuaciones en LaTeX.**
5. **Figuras.**
6. **Tablas.**
7. **Citas IEEE.**

</small>
