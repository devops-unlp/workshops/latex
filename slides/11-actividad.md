# Actividad

---V
## Configuración Inicial:

1. Crea un nuevo archivo LaTeX. 
1. Configura la clase del documento como article.
1. Define el título, autor y fecha de tu documento.

---V
## Estructura del Documento:

1. Divide tu documento en secciones para tres provincias argentinas de tu elección.
1. Agrega una introducción breve al principio del documento.

---V
## Contenido de las Secciones:

En cada sección dedicada a una provincia, proporciona información básica, como:
- Nombre de la provincia.
- Capital de la provincia.
- Población.
- Breve descripción de las características geográficas o culturales.
- Utiliza al menos una ecuación simple relacionada con algún aspecto de la provincia (por ejemplo, la tasa de crecimiento de la población).

---V
## Cita Bibliográfica con BibTeX:

- Agrega una cita bibliográfica utilizando BibTeX en una sección específica al final de tu documento.

---V
## Tabla

- Tabla de Población:
  - Crea una tabla que incluya las tres provincias y su población correspondiente.
  - Resalta la celda de la provincia con la mayor población en color rojo
