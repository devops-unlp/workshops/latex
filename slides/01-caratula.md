<img src=images/LaTeX_project_logo_bird.svg height=150px/>

# Taller de LaTeX

- <a href="https://clubdevops.ar" target="_blank"> Club DevOps </a>

---V

### Atajos Presentación

- Zoom: **`Ctrl+Click`**
- Buscar: **`Ctrl+F`**
- Pantalla Completa: **`Alt+f`**
- Ir a la primera columna: **`Alt+H`**
- Ir a la última fila: **`Alt+J`**
- Ir a la primera fiila: **`Alt+K`**
- Ir a la última columna: **`Alt+L`**
- Vista Previa: **`Alt+O`**
- Exportar a PDF: /?print-pdf
- Ver notas: **`Alt+S`**
