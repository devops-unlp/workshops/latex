# Tablas.

---V
#### **\tabular**
- El entorno `tabular` se utiliza para crear tablas en LaTeX.

```latex
\begin{tabular}{columnas}
    contenido de la tabla
\end{tabular}
```
- `c`(center): Columna centrada.
- `l`(left): Columna alineada a la izquierda.
- `r`(right): Columna alineada a la derecha.
- `|`: Línea vertical entre columnas.

---V
#### **Sintaxis dentro de la tabla**

- `\hline`: Línea horizontal.
- `&`: Separador de columnas.
- `\\`: Cambio de fila.

---V
#### **Ejemplo tabular:**
```latex
\begin{tabular}{|c|c|}
    \hline
    Provincia & Población [M] \\
    \hline
    BsAs & 17,5 \\
    Cdba & 3,97 \\
    SdE & 1,05 \\
    \hline
\end{tabular}
```
---V
#### **Ancho y altura de las celdas**
- Se requiere el paquete **array**
- Al definir las columnas utilizamos la sintaxis pos_vert{ancho}
    - *Ancho* en cm.
    - *pos_vert*: m(middle), b(bottom)

---V
#### **Personalizar tabla**
```latex
\usepackage{array}
\usepackage[table]{xcolor}

\setlength{\tabcolsep}{18pt} %Gap horizontal
\renewcommand{\arraystretch}{1.5} %Gap vertical
\setlength{\arrayrulewidth}{0.5mm} %Ancho del borde
\arrayrulecolor{blue} % Color del borde
```

---V
#### **Combinar celdas.**
- Añadir el paquete **multirow**
- Filas: **\multirow{celdas}{ancho}{Contenido}**
- Columnas: **\multicol{celdas}{ancho}{Contenido}**

```latex
\begin{tabular}{|c|c|c|}
    \hline
    \multicolumn{3}{|c|}{Columna múltiple} \\ \hline
    1 & 2 & 3 \\\hline 
    \multirow{2}{2cm}{Fila múltiple} & 5 & 6\\\cline{2-3}
     & 8 & 9 \\ \hline
\end{tabular}
```
---V
#### **Colorear celdas**
- Se utilizan los comandos **\rowcolor, \cellcolor**
- Se utiliza la sintaxis \...color{color!porcentaje}
- Para columnas se debe crear un nuevo tipo de columna:
```latex
\newcolumntype{s}{>{\columncolor{blue!20}} c}
```
- Se puede alternar el color de las filas:

```latex
\rowcolors{2}{black!30}{black!5}
\begin{tabular}...
```

---V
#### **Ejemplo**

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{array,multirow}
\usepackage[table]{xcolor}

\setlength{\tabcolsep}{18pt}
\renewcommand{\arraystretch}{1.5}
\setlength{\arrayrulewidth}{0.5mm}
\arrayrulecolor{violet}
\newcolumntype{T}{>{\columncolor{green!20}} c}

\begin{document}
\section{Tablas}
\rowcolors{4}{black!20}{black!2}
\begin{tabular}{|l|r|T|}
    \hline
    \rowcolor{blue!15}Provincia & Población en 2022 & --- \\\hline
    --- & --- & ---  \\\hline
    --- & --- & ---  \\\hline
    Bs. Aires & \cellcolor{red!40}17,569,053 & ---  \\
    Córdoba & 3,978,984 & ---  \\
    Santa Fe & 3,556,522 & ---  \\
    CABA & 3,120,612 & ---  \\
    Mendoza & 2,014,533 & ---  \\
    \hline
\end{tabular}
\end{document}
```
