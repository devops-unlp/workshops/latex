# Club DevOps

---V
## ¿ Quienes somos ?

Somos un Club de Ciencia y Tecnología conformado por estudiantes de Ingeniería e Informática. 

---V
## ¿ Que hacemos ?

- Formarnos
- Experimentamos
- Apoyamos
- Promovemos 

---V
## ¿ Por qué Club DevOps ?

Creemos en la educación pública y desde nuestro lugar queremos aportar nuestro granito de arena en la formación de futuros profesionales comprometidos con el País y la soberanía tecnológica.

---V
## Nuestras actividades

---V
### Talleres

- Taller de Git nivel 1 (4 ediciones)
- Taller de Git nivel 2 (1 edición)
- Taller de Bash nivel 1 (1 edición)
- Taller de Bash nivel 2 (Planificado)
- Taller de Latex (1 edición)
- Taller de Docker (Planificado)

---V
### Jornadas

- Jornadas de Instalacion de Linux (2 ediciones)
- Jornadas de mate/café (Siempre :D) 

---V
### Extensión Universitaria

- Proyecto Khoa.

Con la biblioteca popular <a href="https://www.instagram.com/biblioteca.adrianacasajus/" target="_blank">"Adriana Casajús"</a> en el centro cultural <a href="https://www.instagram.com/aluvionlp/" target="_blank">Aluvión</a>

---V
### Infraestructura

- Servidores
- Redes
- Virtualización
- Aplicaciones y servicios
