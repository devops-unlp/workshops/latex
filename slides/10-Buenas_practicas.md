# Buenas Prácticas

---V
#### **Secciones**
- Se pueden crear las secciones en archivos separados del `main.tex` para mantener un proyecto LaTeX organizado y modular.
    - Para añadirlos, utiliza el comando `\input{nombre_del_archivo}`.
- Se puede generar una lista de secciones automáticamente en el documento.
    - Utiliza el comando `\tableofcontents`.

---V
#### **Ecuaciones**
- Utilizá entornos como `equation` o `align` para escribir ecuaciones matemáticas con una buena estructura y numeración automática.
- Añadí etiquetas a las ecuaciones para poder hacer referencia a ellas en el texto con `\label{nombre_etiqueta}`.
- Utiliza `\ref{nombre_etiqueta}` para referenciar ecuaciones en el texto.

---V
#### **Figuras**
- Ajustá el ancho y alto de las figuras utilizando las opciones `[width]` o `[height]` en `\includegraphics`.
    - Ejemplo: `\includegraphics[width=0.7\textwidth]{nombre_del_archivo}`.
- Agregá una descripción clara de la imagen utilizando el comando `\caption`.
- Generá una lista de figuras automáticamente en el documento con `\listoffigures`.

---V
#### **Tablas**
- Utilizá el entorno `table` para hacer que una tabla sea flotante, lo que te permite aprovechar las ventajas de las figuras flotantes.
- Añadí una etiqueta de descripción (`\caption`) a tus tablas para proporcionar contexto.
- Genera una lista de tablas automáticamente en el documento con `\listoftables`.
